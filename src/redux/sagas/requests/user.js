import axios  from "axios";

export function requestGetUser() {  //4. Async API calls
    return axios.request({
        method : "get",
        url : "https://my-json-server.typicode.com/atothey/demo/user"
    });
}