import { call, put } from "redux-saga/effects";
import { setUser } from "../../ducks/user";
import { requestGetUser } from "../requests/user";

export function* handleGetUser(action) {

    try {
        const response = yield call(requestGetUser); // 3. calls  requestGetUser from request to fetch data (Go to request/user.js)
        const { data } = response;  //5. return response and stores data in data 
        yield put(setUser(data));  // 6. dispatch redux action with data (Go to ducks/user.js)
    }catch (error){
        console.log(error);
    }

}