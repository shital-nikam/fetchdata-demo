import { applyMiddleware, createStore, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import userReducer from "./ducks/user";
import { watcherSaga } from './sagas/rootsaga';


const reducer = combineReducers({  // for  multiple reducers
    user : userReducer
});

const sagaMiddleware = createSagaMiddleware();

const middleware = [sagaMiddleware];

const store = createStore (reducer,{},  applyMiddleware(...middleware))

 sagaMiddleware.run(watcherSaga);  // 1.  calls watcherSaga from rootsaga.js (check rootsaga.js)

export default store;
// sagaMiddleware.run(usersSaga);