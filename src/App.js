


import { useSelector, useDispatch } from 'react-redux';
import {useEffect} from 'react';
import { getUser } from './redux/ducks/user';

export default function App() {

  const dispatch = useDispatch(); 

  useEffect(() => {   // after render this calls
    dispatch(getUser()); // 0. dispatch getuser() action to store  (Go to configurestore.js)
  }, [dispatch])

  const user = useSelector((state) => state.user.user);
  console.log(user);

  return (
    <div className="App">
      {user &&   //if loop
      (
        <h2>HI  : {user.firstName} {user.lastName}</h2>
      )}
        {/* <h3> HI, USERNAME IS {user.firstName} </h3> */}
    </div>
  );
}


